package com.adu.music.bean;

/**
 * 歌单种类
 * @author duchuanchuan
 * @date 2016/12/9
 */
public class PlaylistCategory {
    // 种类名称
    private String name;
    // offset 页面偏移量
    private int offset;

    public PlaylistCategory(String name, int offset) {
        this.name = name;
        this.offset = offset;
    }

    public String getName() {
        return name;
    }

    public int getOffset() {
        return offset;
    }

}
