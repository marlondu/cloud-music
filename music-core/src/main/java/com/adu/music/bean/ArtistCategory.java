package com.adu.music.bean;

/**
 * 歌手类型
 * @author duchuanchuan
 * @date 2016/12/9
 */
public class ArtistCategory {
    // 类型id
    private int id;
    // 类型名称
    private String name;
    // 代表字母 a-z 和 0
    private int initial;

    public ArtistCategory(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setInitial(int initial) {
        this.initial = initial;
    }

    public int getInitial() {
        return this.initial;
    }
}
