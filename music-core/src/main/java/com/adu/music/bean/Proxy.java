package com.adu.music.bean;

import com.adu.music.util.CommonUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author duchuanchuan
 * @date 2017/1/6
 */
public class Proxy {

    private int id;
    private String ip;
    private int port;
    private String type;
    private String getpost;
    private double speed;
    private int priority;
    private boolean checked;

    public Proxy() {
    }

    public Proxy(String ip, int port, String type, String getpost, double speed) {
        this.ip = ip;
        this.port = port;
        this.type = type;
        this.getpost = getpost;
        this.speed = speed;
        this.priority = CommonUtils.judgePorxyPriority(this.speed);
        this.checked = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
        if(isAvailable())
            this.priority = CommonUtils.judgePorxyPriority(this.speed);
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setGetpost(String getpost) {
        this.getpost = getpost;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String getType() {
        return type;
    }

    public String getGetpost() {
        return getpost;
    }

    public double getSpeed() {
        return speed;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    /**
     * 设置为不可用
     */
    public void setUnavailable() {
        this.priority = -1;
    }

    /**
     * 代理是否可用
     * @return 是否可用
     */
    public boolean isAvailable() {
        return this.priority > 0 && port > 0;
    }

    /**
     * 是否支持https
     * @return true or false
     */
    public boolean supportHttps() {
        return StringUtils.contains(this.type, "HTTPS");
    }

    /**
     * 是否支持post请求
     * @return true or false
     */
    public boolean supportPost() {
        return StringUtils.contains(this.getpost, "POST");
    }

    @Override
    public int hashCode() {
        if (StringUtils.isBlank(ip))
            return super.hashCode();
        String proxy = ip + ":" + port;
        return proxy.hashCode();
    }

    @Override
    public String toString() {
        return "Proxy{" +
                "id=" + id +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                ", type='" + type + '\'' +
                ", getpost='" + getpost + '\'' +
                ", speed=" + speed +
                ", priority=" + priority +
                '}';
    }
}
