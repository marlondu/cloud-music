package com.adu.music.parser;

import com.adu.music.bean.Proxy;

import java.util.List;

/**
 * @author duchuanchuan
 * @date 2017/1/7
 */
public interface ProxyParser {

    /**
     * 获取最新的代理
     * @return 代理集合
     */
    List<Proxy> parseProxyList();
}
