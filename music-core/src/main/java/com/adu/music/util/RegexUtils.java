package com.adu.music.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author duchuanchuan
 * @date 2016/12/4
 */
public class RegexUtils {

    private RegexUtils() {
    }

    public static String getMatchedString(String regex, String str, int flag) {
        Pattern p = Pattern.compile(regex);
        Matcher match = p.matcher(str);

        String result = "";
        if (match.find()) {
            result = match.group(flag);
        }

        return result;
    }

    public static boolean isMatchedString(String regex, String str) {
        Pattern p = Pattern.compile(regex);
        Matcher match = p.matcher(str);
        return match.matches();
    }

    public static String replaceSpecialChars(String str) {
        String regex = "[()*/:?]+";
        str = str.replaceAll(regex, "");
        return str;
    }
}
