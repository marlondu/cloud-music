package com.adu.music.util;

import com.adu.music.db.AsyncNamedParameterJdbcTemplate;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * @author duchuanchuan
 * @date 2016/12/3.
 */
public class DbUtils {

    private static final Logger logger = LoggerFactory.getLogger(DbUtils.class);

    // 获取数据库连接池
    private static synchronized ComboPooledDataSource getDataSource(String username, String pwd, String url,
                                                                    String driver) {
        ComboPooledDataSource test = new ComboPooledDataSource();
        try {
            test.setUser(username);
            test.setPassword(pwd);
            test.setJdbcUrl(url);
            test.setDriverClass(driver);
            test.setInitialPoolSize(3);
            test.setMinPoolSize(3);
            test.setMaxPoolSize(10);
            test.setMaxStatements(10);
            test.setMaxIdleTime(5000);
            test.setTestConnectionOnCheckout(true);
            test.setTestConnectionOnCheckin(true);
        } catch (Exception e) {
            logger.error("数据库初始化过程中发生严重错误", e);
            System.exit(1);
        }
        return test;
    }

    private static ComboPooledDataSource dataSource;
    private static ComboPooledDataSource remoteDataSource;

    private synchronized static ComboPooledDataSource getDataSource() {
        if (dataSource == null) {
            dataSource = getDataSource(ConfigManager.getUsername(), ConfigManager.getPassword(),
                    ConfigManager.getJdbcUrl(), ConfigManager.getDriverClass());
        }
        return dataSource;
    }

    private static final class JdbcTemplateHolder {
        private static final JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
    }

    public static JdbcTemplate getJdbcTemplate() {
        return JdbcTemplateHolder.jdbcTemplate;
    }

    private static final class NamedParameterJdbcTemplateHolder {
        private static final NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(getDataSource());
    }

    public static NamedParameterJdbcTemplate getNamedPamaterJdbcTemplate() {
        return NamedParameterJdbcTemplateHolder.namedParameterJdbcTemplate;
    }

    private static final class AsyncNamedParamterJdbcTemplateHolder {
        private static final AsyncNamedParameterJdbcTemplate asyncNamedParamterJdbcTemplate = new AsyncNamedParameterJdbcTemplate(getDataSource());
    }

    public static AsyncNamedParameterJdbcTemplate getAsyncNamedPamaterJdbcTemplate() {
        return AsyncNamedParamterJdbcTemplateHolder.asyncNamedParamterJdbcTemplate;
    }

    /**
     * 远程数据库数据源
     * @return 远程数据库数据源
     */
    private synchronized static ComboPooledDataSource getRemoteDataSource() {
        if (remoteDataSource == null) {
            remoteDataSource = getDataSource(ConfigManager.getRemoteUsername(), ConfigManager.getRemotePassword(),
                    ConfigManager.getRemoteJdbcUrl(), ConfigManager.getRemoteDriverClass());
        }
        return remoteDataSource;
    }

    private static final class RemoteNamedParameterJdbcTemplateHolder {
        private static final NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(getRemoteDataSource());
    }

    public static NamedParameterJdbcTemplate getRemoteNamedPamaterJdbcTemplate() {
        return RemoteNamedParameterJdbcTemplateHolder.namedParameterJdbcTemplate;
    }
}
