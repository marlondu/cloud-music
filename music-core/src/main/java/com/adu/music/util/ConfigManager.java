package com.adu.music.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
/**
 * @author duchuanchuan
 * @date 2016/12/3.
 */
public class ConfigManager {

    private static final Config commonConfig = ConfigFactory.load("common.conf");

    public static String getJdbcUrl() {
        return commonConfig.getString("db.jdbc_url");
    }

    public static String getDriverClass() {
        return commonConfig.getString("db.driver_class");
    }

    public static String getUsername() {
        return commonConfig.getString("db.user_name");
    }

    public static String getPassword() {
        return commonConfig.getString("db.password");
    }

    public static String getRemoteJdbcUrl() {
        return commonConfig.getString("db_remote.jdbc_url");
    }

    public static String getRemoteDriverClass() {
        return commonConfig.getString("db_remote.driver_class");
    }

    public static String getRemoteUsername() {
        return commonConfig.getString("db_remote.user_name");
    }

    public static String getRemotePassword() {
        return commonConfig.getString("db_remote.password");
    }

    public static boolean getUseProxy() {
        return commonConfig.getBoolean("common.use_proxy");
    }

    public static LocalTime getSpiderTime(){
        String spiderTimeConfig = commonConfig.getString("spider.start_time");
        return LocalTime.parse(spiderTimeConfig, DateTimeFormatter.ofPattern("HH:mm:ss"));
    }

}
