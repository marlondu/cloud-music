package com.adu.music.proxy;

import com.adu.music.bean.Proxy;

/**
 * @author duchuanchuan
 * @date 2017/1/6
 */
public abstract class AbstractProxyPool implements ProxyPool {

    @Override
    public final void release(Proxy proxy) {
        if (proxy.isAvailable()) returnToPool(proxy);
        else remove(proxy);
    }

    protected abstract void returnToPool(Proxy proxy);

    protected abstract void remove(Proxy proxy);
}
