package com.adu.music.proxy;

import com.adu.music.bean.Proxy;

/**
 * 代理池，负责提供可用的代理
 * @author duchuanchuan
 * @date 2017/1/6
 */
public interface ProxyPool {
    /**
     * 获取一个新的代理
     * @return 一个新的代理
     */
    Proxy getProxy();

    /**
     * 释放某个代理，数据库设为不可用
     * @param proxy 代理
     */
    void release(Proxy proxy);

    /**
     * 关闭代理池，将代理更新到数据库，释放资源
     */
    void shutdown();

    /**
     * 打开代理池
     */
    void open();

}
