package com.adu.music.proxy;

/**
 * @author duchuanchuan
 * @date 2017/1/8
 */
public class ProxyPoolFactory {

    private ProxyPoolFactory() {
    }

    /**
     * 创建非阻塞的代理池
     * @return
     */
    public static ProxyPool createUnBlockedProxyPool() {
        return new UnBlockedProxyPool();
    }

    public static ProxyPool createUnBlockedProxyPool(int capacity) {
        return new UnBlockedProxyPool(capacity);
    }
}
