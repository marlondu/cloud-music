package com.adu.music.proxy;

import com.adu.music.bean.Proxy;
import com.adu.music.db.ProxyDao;
import com.adu.music.parser.KuaidailiParser;
import com.adu.music.parser.XicidailiParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author duchuanchuan
 * @date 2017/1/5
 */
public class ProxySpider implements Runnable {

    private ProxyDao proxyDao = ProxyDao.getInstance();
    private Logger logger = LoggerFactory.getLogger(ProxySpider.class);

    private void updateProxy() {
        // 快代理
        List<Proxy> proxyList = new KuaidailiParser().parseProxyList();
        // xicidaili
        proxyList.addAll(new XicidailiParser().parseProxyList());
        proxyList = ProxyChecker.filterAvailable(proxyList).parallelStream()
                .filter(this::notExist).collect(Collectors.toList());
        logger.info("get available proxy {} items", proxyList.size());
        proxyDao.batchSave(proxyList);
    }

    private boolean notExist(Proxy proxy) {
        return !proxyDao.hashcodeExist(proxy.hashCode());
    }

    @Override
    public void run() {
        updateProxy();
    }
}
