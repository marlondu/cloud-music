package com.adu.music.api;

import com.adu.music.bean.ArtistCategory;
import com.adu.music.bean.PlaylistCategory;
import com.adu.music.parser.ArtistParser;
import com.adu.music.parser.PlaylistParser;
import com.adu.music.parser.SongParser;
import com.adu.music.util.JsoupUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author duchuanchuan
 * @date 2016/12/2.
 */
public class MusicApi {

    private static final Logger logger = LoggerFactory.getLogger(MusicApi.class);

    /**
     * 抓取歌手信息列表
     * @param cate 歌手种类
     * @return 歌手信息列表
     */
    public static List<Map<String, Object>> crawlArtist(ArtistCategory cate) {
        try {
            return ArtistParser.parseArtistList(cate);
        } catch (Exception e) {
            logger.error("抓取歌手信息出现异常");
            throw new RuntimeException(e);
        }
    }

    /**
     * 抓取热门歌单
     * @param category http://music.163.com/discover/playlist/?order=hot&cat=华语&limit=35&offset=35
     * @return 歌单信息列表
     */
    public static List<Map<String, Object>> crawlHotPlayLists(PlaylistCategory category) {
        try {
            return PlaylistParser.parsePlaylists(category);
        } catch (Exception e) {
            logger.error("抓取歌单列表信息异常:{}", e);
            throw new RuntimeException(e);
        }

    }

    public static void initPlayListcxCategories(List<String> categories) {
        String url = "http://music.163.com/discover/playlist";
        Optional<Document> docOptional = JsoupUtils.get(url);
        docOptional.ifPresent(doc -> {
            Elements cateElements = doc.getElementsByClass("s-fc1 ");
            for (Element cate : cateElements) {
                categories.add(cate.text());
            }
        });
    }


    /**
     * 根据歌单ID抓取其包含的歌曲
     * @param playlistId 歌单ID
     * @return 该歌单下包含的歌曲
     */
    public static List<Map<String, Object>> crawlSongList(String playlistId) {
        List<String> songIds = PlaylistParser.parseSongIds(playlistId);
        final List<Map<String, Object>> list = new ArrayList<>();
        songIds.forEach(songId -> {
            try {
                Map<String, Object> song = SongParser.parseSong(songId);
                if (!song.isEmpty()) list.add(song);
            } catch (Exception e) {
                logger.error("歌曲ID: {}抓取异常", songId);
            }

        });
        return list;
    }

    /**
     * 根据歌手ID抓取top50歌曲
     * @param artistId 歌手id
     * @return 歌曲列表
     */
    public static List<Map<String, Object>> crawlTop50Songs(String artistId) {
        List<String> songIds = ArtistParser.parseTo50SongIds(artistId);
        List<Map<String, Object>> songs = new ArrayList<>();
        songIds.forEach(id -> {
            Map<String, Object> song = SongParser.parseSong(id);
            if (!song.isEmpty()) songs.add(song);
        });
        return songs;
    }

}
