CREATE TABLE artist
(
    id INT(11) PRIMARY KEY NOT NULL,
    name VARCHAR(100) NOT NULL,
    category_id INT(11),
    category_name VARCHAR(20),
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP
);
CREATE UNIQUE INDEX artist_id_uindex ON artist (id);
CREATE TABLE mv
(
    id INT(11) PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL,
    song_id INT(11),
    artist_id INT(11),
    artist_name VARCHAR(255),
    comment INT(11) DEFAULT '0',
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
);
CREATE TABLE playlist
(
    id INT(11) PRIMARY KEY NOT NULL,
    name VARCHAR(100) NOT NULL,
    category VARCHAR(50) COMMENT '标签,多个逗号分割',
    favorite INT(11) DEFAULT '0' NOT NULL,
    share INT(11) DEFAULT '0' NOT NULL COMMENT '分享数量',
    comment INT(11) DEFAULT '0' NOT NULL COMMENT '评论数量',
    description VARCHAR(1000) COMMENT '介绍',
    user_id INT(11) NOT NULL COMMENT '创建者id',
    user_name VARCHAR(100) COMMENT '创建者名称',
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    play INT(11) DEFAULT '0' COMMENT '播放次数'
);
CREATE TABLE proxy
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    ip VARCHAR(50) NOT NULL,
    port INT(11) NOT NULL,
    speed DOUBLE DEFAULT '0' NOT NULL,
    priority INT(11) NOT NULL,
    getpost VARCHAR(50),
    type VARCHAR(50),
    hashcode INT(11) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
);
CREATE TABLE song
(
    id INT(11) PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL,
    artist_id VARCHAR(255) COMMENT '所属的歌手的id, 可能是多个，是的话逗号分隔',
    artist_name VARCHAR(255) COMMENT '所属歌手的名字',
    album_id INT(11) COMMENT '所属专辑id',
    album_name VARCHAR(255),
    comment INT(11) NOT NULL COMMENT '评论数量',
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
);
CREATE INDEX comment_index ON song (comment);
CREATE INDEX artistid_index ON song (artist_id);